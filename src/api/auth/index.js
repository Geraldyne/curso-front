import axios from 'axios';
import { apiUrl } from '../apiURL';

export default {
  async login(params) {
    return axios.post(`${apiUrl}/login`, params);
  },
};
