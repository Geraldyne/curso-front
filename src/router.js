import Vue from 'vue'
import Router from 'vue-router'
import store from './store.js'
import axios from 'axios';
import vueNotification from '@kugatsu/vuenotification';

import { apiUrl } from '@/api/apiURL';


Vue.prototype.$notification = vueNotification;


const ifCliente = (to, from, next) => {
  if (store.getters.isCliente) {
    next()
    return
  }

  Vue.prototype.$notification.error("No tiene permiso para acceder a esta página");

  next('/inicio')
}



const ifAdministrador = (to, from, next) => {
  if (store.getters.isAdmin) {
    next()
    return
  }
  
  Vue.prototype.$notification.error("No tiene permiso para acceder a esta página");

  next('/inicio')
}



const ifPuedeVerCurso = (to, from, next) => {

  const curso_id = to.params.id;

  if (store.getters.isAdmin) {
    next()
    return
  }
  else{

    const user = store.getters.currentUser

    axios({ url: `${apiUrl}/api/mi-curso/${curso_id}`, data: user, method: 'GET' })
      .then(resp => {

        console.log(resp);

        const suscripcion = resp.data.curso

        if(suscripcion){
          
          next()
          return

        }else{

          Vue.prototype.$notification.error("No puede acceder a esta página en este momento");

          next('/inicio')

        }
        
      })
      .catch(err => {
        console.log(err)

        Vue.prototype.$notification.error("No puede acceder a esta página en este momento");

        next('/inicio')

      })

  }

  
}


Vue.use(Router)
let router = new Router({
  mode: 'history',
  routes: [
      // =============================================================================
      // AUTHENTICATION
      // =============================================================================
      {
        path: '',
        component: () => import('@/layouts/full-page/Fullpage.vue'),
        children: [
          /*
          *  Login
          */ 
          {
            path: '/',
            name: 'login',
            component: () => import('@/views/auth/Login.vue')
          },   
        ]
      },
      // =============================================================================
      // ADMIN
      // =============================================================================    
      {
        path: '',
        component: () => import('./layouts/main/Main.vue'),
        children: [
    
          {
            path: '/inicio',
            name: 'inicio',
            component: () => import('./views/Dashboard.vue'),
            meta: { 
              requiresAuth: true
            }
          },
      // =============================================================================
      // CURSOS
      // ============================================================================= 
          {
            path: '/cursos',
            name: 'cursos',
            component: () => import('./views/cursos/CursosList.vue'),
            meta: { 
              requiresAuth: true
            }
          },
          {
            path: '/cursos/crear',
            name: 'crearcursos',
            component: () => import('./views/cursos/CursosCreate.vue'),
            meta: { 
              requiresAuth: true
            },
            beforeEnter: ifAdministrador
          },
          {
            path: '/cursos/editar/:id',
            name: 'editarcursos',
            component: () => import('./views/cursos/CursosEdit.vue'),
            meta: { 
              requiresAuth: true
            },
            beforeEnter: ifAdministrador
          },
          {
            path: '/cursos/:id',
            name: 'detallescursos',
            component: () => import('./views/cursos/CursosDetalles.vue'),
            meta: { 
              requiresAuth: true
            },
            beforeEnter: ifPuedeVerCurso
          },
      // =============================================================================
      // Profesores
      // ============================================================================= 
          {
            path: '/profesores',
            name: 'profesores',
            component: () => import('./views/profesores/ProfesoresList.vue'),
            meta: { 
              requiresAuth: true
            },
            beforeEnter: ifAdministrador
          },
          {
            path: '/profesores/crear',
            name: 'crearprofesores',
            component: () => import('./views/profesores/ProfesoresCreate.vue'),
            meta: { 
              requiresAuth: true
            }
          },
          {
            path: '/profesores/editar/:id',
            name: 'editarprofesores',
            component: () => import('./views/profesores/ProfesoresEdit.vue'),
            meta: { 
              requiresAuth: true
            },
            beforeEnter: ifAdministrador
          },
      // =============================================================================
      // Profesores
      // ============================================================================= 
      {
        path: '/mis-cursos',
        name: 'miscursos',
        component: () => import('./views/clientes/MisCursos.vue'),
        meta: { 
          requiresAuth: true
        },
        beforeEnter: ifCliente
      },

        ],

      },


  ]
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
      if (store.getters.isLoggedIn) {
        next()
        return
      }
      next('/')
    } else {
      next()
    }
})

export default router 