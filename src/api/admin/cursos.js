import axios from 'axios';
import { apiUrl } from '../apiURL';

export default {
  async cursos(categoria,params) {
    return axios.get(`${apiUrl}/api/cursos?categoria=${categoria}`, params);
  },
  async create(params) {
    return axios.post(`${apiUrl}/api/cursos`, params);
  },
  async show(id,params) {
    return axios.get(`${apiUrl}/api/cursos/${id}`, params);
  },
  async update(id, params) {
    return axios.put(`${apiUrl}/api/cursos/${id}`, params);
  },
    async delete(id, params) {
    return axios.delete(`${apiUrl}/api/cursos/${id}`, params);
  }
};