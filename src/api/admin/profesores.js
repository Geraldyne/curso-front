import axios from 'axios';
import { apiUrl } from '../apiURL';

export default {
  async profesores(params) {
    return axios.get(`${apiUrl}/api/profesores`, params);
  },
  async create(params) {
    return axios.post(`${apiUrl}/api/profesores`, params);
   },
   async show(id,params) {
     return axios.get(`${apiUrl}/api/profesores/${id}`, params);
   },
   async update(id, params) {
     return axios.put(`${apiUrl}/api/profesores/${id}`, params);
   },
   async delete(id, params) {
    return axios.delete(`${apiUrl}/api/profesores/${id}`, params);
   }
};