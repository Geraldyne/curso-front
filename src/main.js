import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Axios from 'axios'
import jQuery from 'jquery'
import vueNotification from '@kugatsu/vuenotification'
import VTooltip from 'v-tooltip'

Vue.use(VTooltip)



//---------JS---------//
import '@/assets/plugins/jquery/jquery.min.js';

import '@/assets/plugins/jquery-knob/jquery.knob.min.js';

import '@/assets/plugins/jquery-ui/jquery-ui.min.js';

import '@/assets/plugins/bootstrap/js/bootstrap.bundle.min.js';

import '@/assets/plugins/datatables/jquery.dataTables.js';

import '@/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js';

import '@/assets/plugins/bs-custom-file-input/bs-custom-file-input.js';


//---------CSS---------//


import '@/assets/css/adminlte.css';

import '@/assets/plugins/fontawesome-free/css/all.css';

import '@/assets/plugins/fontawesome-free/css/all.min.css';

import '@/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css';

import '@/assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css';

import '@/assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css';

import '@/assets/plugins/jqvmap/jqvmap.min.css';

import '@/assets/plugins/daterangepicker/daterangepicker.css';

import '@/assets/plugins/summernote/summernote.css';

import '@/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css';



// Config Axios
import interceptorsSetup from '@/helpers/axios/config'
// and running it somewhere here
interceptorsSetup()


Vue.use(vueNotification, {
  position: 'bottomRight',
  timer: 10
})

global.jQuery = jQuery
global.$ = jQuery



Vue.config.productionTip = false

Vue.prototype.$http = Axios;

const token = sessionStorage.getItem('token');
if (token) {
  console.log("aaaaaaaaaaaaa");
  console.log(token)
  Vue.prototype.$http.defaults.headers.common['Authorization'] = 'Bearer ' + token
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
