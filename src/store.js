import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { apiUrl } from '@/api/apiURL'

import { getLocalUser } from '@/helpers/auth.js'

const user = getLocalUser();

Vue.use(Vuex)


export default new Vuex.Store({
  state: {
    status: '',
    token: sessionStorage.getItem('token') || '',
    InfoUser: {
      name: '',
      rol: ''
    },
    currentUser: user,
  },
  mutations: {
    auth_request(state) {
        state.status = 'loading'
    },
    auth_success(state, token) {
      state.status = 'success'
      state.token = token  
    },
    auth_error(state) {
      state.status = 'error'
    },
    logout(state) {
      state.status = ''
      state.token = ''
    },
    set_current_user(state, user){
      state.user = user
      state.InfoUser.name = user.name
      state.InfoUser.rol = user.rol

      // sessionStorage.setItem('user',user)

      state.currentUser = Object.assign({}, user);

      sessionStorage.setItem("user", JSON.stringify(state.currentUser));

    },
  },
  actions: {
    //Establecemos la conexion y alamacenamos el token
    login({ commit }, user) {
      return new Promise((resolve) => {
        commit('auth_request')
        axios({ url: `${apiUrl}/api/login`, data: user, method: 'POST' })
          .then(resp => {
            
            const token = resp.data.token
            
            sessionStorage.setItem('token', token)
            // Add the following line:
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + token

            commit('auth_success', token)

            resolve(resp)
          })
          .catch(err => {
            commit('auth_error')
            sessionStorage.removeItem('token')
            console.log(err)
          })
      }) 
    },
    logout({ commit }) {
      return new Promise((resolve) => {
        commit('logout')
        sessionStorage.removeItem('token')
        delete axios.defaults.headers.common['Authorization']
        resolve()
      })
    },
    setUser ({ commit }, user) {
      commit('set_current_user',user)
    },
  },
  getters: {
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status,
    currentUser(state){
      return   state.currentUser;
    },
    isCliente(state){
      if(state.currentUser)
        return state.currentUser.rol == 'cliente' ? true : false;
      
      return false;
    }
    ,
    isAdmin(state){
      if(state.currentUser)
        return state.currentUser.rol == 'administrador' ? true : false;

      return false;
    }
  }
})