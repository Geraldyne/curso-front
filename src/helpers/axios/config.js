import axios from 'axios'
/*import router from '../../router'
import i18n from './vue-i18n'*/


export default function setup() {

    axios.interceptors.request.use(function(config) {

        axios.defaults.headers.common['Content-Type'] = 'application/json'

        const token = localStorage.getItem('access_token')
       
        if(token) config.headers.Authorization = `Bearer ${token}`
        
        return config

    }, function(err) {

        return Promise.reject(err)

    });

    axios.interceptors.response.use(response => response, error => {

      // const { data } = error.response

      // console.log(data)

      return Promise.reject(error)
    })
}
