import axios from 'axios';
import { apiUrl } from '../apiURL';

export default {
  async miscursos(params) {
    return axios.get(`${apiUrl}/api/mis-cursos`, params);
  },
  async suscribir(params) {
    return axios.post(`${apiUrl}/api/suscribirme`, params);
  },
  async cancelarSuscripcion(params) {
    return axios.post(`${apiUrl}/api/cancelar-suscripcion/`, params);
  },
};